module DnaKataTest
open FsUnit.Xunit
open Xunit
open Swensen.Unquote
open DnaKata

[<Fact>]
let ``Delete invalid pair``() = 
    let invalidSequence = "TA@T!CXYZPOILG"
    test <@ clean invalidSequence = "TATCG" @>

[<Fact>]
let ``Reverse given sequence``() =
    test <@ "TAGTC" |> reverse = "CTGAT" @>

[<Fact>]
let ``Parse to command and sequence``() =
    test <@ "reverse\nTAGTC" 
    |> parse = Success { Command = Reverse; Sequence = "TAGTC" } @>
    
[<Fact>]
let ``Parse invalid command and sequence``() =
    test <@ "nimportekoi\nTAGTC" 
    |> parse = Failure @>

[<Fact>]
let ``Valid if valid nucleobase``() =
    test <@ validateNucleobase { Command = Reverse; Sequence = "ATCG" } = Success { Command = Reverse; Sequence = "ATCG" } @>

[<Fact>]
let ``Invalid if nucleobase sequence is empty``() =
    test <@ validateNucleobase { Command = Reverse; Sequence = "" } = Failure @>

[<Fact>]
let ``Execute expected command for given sequence``() =
    test <@ "reverse\nTAGTXFZC" |> processDna = Success "CTGAT" @>