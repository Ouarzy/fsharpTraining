module DnaKata
let nucleobases = [|'A';'C';'T';'G'|]
let toString (x:seq<char>) = System.String.Concat(x)
let containNucleobases x = Array.contains x nucleobases
let clean sequence =
    sequence
    |> Seq.filter containNucleobases
    |> toString

let reverse (sequence:string) :string  =
    sequence
    |> Seq.rev
    |> toString

type Result<'T> = 
    | Success of 'T
    | Failure

type Process = { Command: Command; Sequence: string }
and Command = Reverse

let toCommand (fileContent:string) =
    fileContent.Split('\n').[0]

let toSequence (fileContent:string) =
    fileContent.Split('\n').[1]

let parse (fileContent:string) = 
    match fileContent |> toCommand with
    | "reverse" -> Success {Command = Reverse ; Sequence = fileContent |> toSequence }
    | _ -> Failure

let validateNucleobase (process: Process) : Result<Process> =
    match process.Sequence with
    | "" -> Failure
    | _ -> Success process

let bind func result =
    match result with 
    | Success p -> func p
    | Failure -> Failure 

let (>>=) f1 f2 = f1 >> bind f2
let ifSuccess func p =
    func p.Sequence |> Success

let processDna =
    parse 
    >>= validateNucleobase
    >>= ifSuccess (clean >> reverse)
