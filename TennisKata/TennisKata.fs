module TennisKata

type PointsData = { 
    Player1Score: PlayerScore
    Player2Score: PlayerScore  
}

and GameScore = 
    | Game of Player
    | Points of PointsData
and Player = Player1 | Player2
and PlayerScore = Love | Fifteen | Thirty | Fourty

let winPoint = function
    | Love -> Fifteen
    | Fifteen -> Thirty
    | Thirty -> Fourty

let winPoints player (currentScore:PointsData):PointsData =
    match currentScore, player with
    | points, Player1 ->
        {points with Player1Score = points.Player1Score |> winPoint }
    | points, Player2 ->
        {points with Player2Score = points.Player2Score |> winPoint }
let win player = function 
    | Points {Player1Score = Fourty; Player2Score = Love} -> Game Player1  
    | Points points -> 
        winPoints player points |> Points

module TennisKataTests =
    open FsUnit.Xunit
    open Xunit
    open Swensen.Unquote

    let point (p1, p2) = { Player1Score = p1; Player2Score = p2 } 

    [<Fact>]
    let ``Given Love Love When Player 1 win Then 15 Love``() =
       let gameScore = Love, Love 
       test <@ gameScore |> point |> winPoints Player1 = point (Fifteen, Love) @>

    [<Fact>]
    let ``Given 15 Love When Player 1 win Then 30 Love``() =
       let gameScore = Fifteen, Love
       test <@ gameScore |> point |> winPoints Player1 = point (Thirty, Love) @>
       
    [<Fact>]
    let ``Given 30 Love When Player 1 win Then 40 Love``() =
       let gameScore = Thirty, Love
       test <@ gameScore |> point |> winPoints Player1 = point (Fourty, Love) @>

    [<Fact>]
    let ``Given 40 Love When Player 1 win Then Game Player 1``() =
       let gameScore:GameScore = (Fourty, Love) |> point |> Points
       test <@ gameScore |> win Player1 = Game Player1 @>
       
    [<Fact>]
    let ``Given Love Love When Player 2 win Then Love 15``() =
       let gameScore = (Love, Love) |> point |> Points
       test <@ gameScore |> win Player2 = ((Love, Fifteen) |> point|> Points) @>

    [<Fact>]
    let ``Given 15 15 When Player 2 win Then 15 30``() =
       let gameScore = (Fifteen, Fifteen)|> point|> Points
       test <@ gameScore |> win Player2 = (Points <| point (Fifteen, Thirty)) @>